<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "contacts.preview",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/contacts_preview.php"
    )
);
?>

    <div class="contacts">
        <div class="row">
            <div class="contacts__block col-xl-6 col-lg-6 col-md-12">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "contacts.area",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/contact_area.php"
                    )
                );
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "feedback.contacts",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/feedback_contacts.php"
                    )
                );
                ?>
            </div>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "contacts.info",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/contacts_info.php"
                )
            );
            ?>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>