<?
use enum\Helper;
global $APPLICATION;
$result = Helper::getOGinfo(CMain::GetCurDir());
$sSectionName = "Контакты";
$arDirProperties = Array(
    "title" => $result["TITLE"],
    "description" => $result["DESCRIPTION"],
    "keywords_inner" => "Связь, контакты, инста, instagram, найти, карта, адрес, телефон",
    "og:title" => $result["TITLE"],
    "og:image" => $result["IMAGE"],
    "og:image:secure_url" => $result["IMAGE_SECURE"],
    "og:image:type" => "image/jpeg",
    "og:image:width" => $result["WIDTH"],
    "og:image:height" => $result["HEIGHT"],
    "og:description" => $result["DESCRIPTION"],
    "og:site_name" => "ENUM.BY",
    "og:url" => $result["URL"],
    "og:locale" => "ru_RU",
    "og:type" => "website",
);
?>