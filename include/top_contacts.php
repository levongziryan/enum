<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<div class="header">
    <a class="header__phone" href="tel:"><?= GetMessage("PHONE"); ?></a>
    <a class="header__email" href="mailto:<?= COption::GetOptionString("main", "email_from")?>"><?= COption::GetOptionString("main", "email_from")?></a>
    <a class="header__send" href="" data-toggle="modal" data-target="#ModalCenter"><?= GetMessage("SEND_REQUEST"); ?></a>
</div>