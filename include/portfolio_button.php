<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<div class="header__logo sidebar__logo navbar-brand"><?= GetMessage("COMPANY"); ?>
    <div class="menu">
        <div class="menu-btn fixed-top"><span></span> </div>
    </div>
</div>