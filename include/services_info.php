<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<div class="pre-testimonials">
    <h2><?= GetMessage("TRUST_US"); ?><br>
        <span><?= GetMessage("COMPANY"); ?></span></h2>
    <p>
        <?= GetMessage("BE_SHURE"); ?>
    </p>
</div>