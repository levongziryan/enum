<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<button class="sidebar__button btn" type="button" data-toggle="modal" data-target="#ModalCenter">
    <?= GetMessage("REQUEST_ORDER"); ?>
</button>
<div class="sidebar__socials">
    <a href="https://www.instagram.com/enum.by/" class="instagram"><i class="fab fa-instagram"></i></a>
</div>
<footer>
    <p><?= GetMessage("FOOTER");?>
    </p>
</footer>