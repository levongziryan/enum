<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>
<div class="map contacts__map col-xl-6 col-lg-6 col-md-12">
    <div class="blocker"></div>
    <div id="map">
        <div class="location">
            <h3><?=GetMessage("FIND_US");?>?</h3>
            <p class="location__address"><?=GetMessage("ADDRESS");?></p>
            <p class="location__phone"><?=GetMessage("PHONE");?></p>
        </div>
    </div>
</div>