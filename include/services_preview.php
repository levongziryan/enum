<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<div class="header__logo sidebar__logo navbar-brand"><?= GetMessage("COMPANY"); ?>
    <div class="menu">
        <div class="menu-btn fixed-top">
            <span></span>
        </div>
    </div>
</div>
<div class="services-hero d-flex">
    <h1><?= GetMessage("SERVICES"); ?>
    </h1>
    <h2><?= GetMessage("OUR_SERVICES"); ?></h2>
</div>