<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<h1><?=GetMessage("OUR_CONTACTS");?></h1>
<h2><?=GetMessage("COME_TO_US");?></h2>
<h3><?=GetMessage("ADDRESS");?>:</h3>
<p><?=GetMessage("ADDRESS_CURRENT");?></p>
<h3><?=GetMessage("GRAPHIC");?>:</h3>
<p><?=GetMessage("GRAPHIC_CURRENT");?></p>
<h3><?=GetMessage("PHONES");?>:</h3>
<p><?=GetMessage("PHONES_CURRENT");?></p>
<h3><?=GetMessage("EMAIL");?>:</h3>
<a href="mailto:<?= COption::GetOptionString("main", "email_from")?>"><?= COption::GetOptionString("main", "email_from")?></a>
