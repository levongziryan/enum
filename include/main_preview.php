<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<div class="header__logo sidebar__logo navbar-brand"><?= GetMessage("COMPANY"); ?>
    <div class="menu">
        <div class="menu-btn fixed-top">
            <span></span>
        </div>
    </div>
</div>
<div class="hero d-flex">
    <h2 class="hero__h2"><?= GetMessage("SOLUTIONS"); ?></h2>
    <h1><?= GetMessage("DEVELOP"); ?>
    </h1>
    <button class="hero__button" data-toggle="modal" data-target="#ModalCenter"><?= GetMessage("ORDER"); ?></button>
    <a href="/portfolio/"><?= GetMessage("PORTFOLIO"); ?></a>
</div>