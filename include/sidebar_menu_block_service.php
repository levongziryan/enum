<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<nav class="sidebar fixed-top">

    <div class="sidebar__image-holder">
        <a>
            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/enum-logo.svg">
        </a>
    </div>
    <? $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "sidebar_menu",
        Array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "left",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => array(""),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "main",
            "USE_EXT" => "N"
        )
    ); ?>
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "header.contacts",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => "/include/header_contacts_service.php"
        )
    );
    ?>
</nav>