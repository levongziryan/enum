<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>

<div class="who-we-are">
    <div class="adaptive-box">
        <div class="row">
            <div class="col-xl-7 col-lg-12 col-md-12 ">
                <h2><?= GetMessage("WE_ARE"); ?>?</h2>
                <p> <?= GetMessage("ENUM_ABOUT_TOP"); ?>
                    <br><br><br>
                    <?= GetMessage("ENUM_ABOUT_BOTTOM"); ?>
                </p>
            </div>
            <div class="col-xl-4 offset-xl-1 col-lg-12  col-md-12 ">
                <p class="right_block_header"><?= GetMessage("HAVE_A_QUESTION"); ?>?</p>
                <p class="advantages__paragraph_right"><?= GetMessage("ASK_QUESTION"); ?></p>
                <a class="btn advatages__button" data-toggle="modal" data-target="#ModalCenter"><?= GetMessage("QUESTION_REQUEST"); ?></a>
            </div>
        </div>
    </div>
</div>
<div class="advantages">
    <div class="adaptive-box">
        <div class="row">
            <div class="col-xl-7 col-lg-12 col-md-12 ">
                <h2><?= GetMessage("CLIENT_ADVANTAGES"); ?></h2>
                <h3><?= GetMessage("MADE_BY_PROFY"); ?></h3>
                <p class="advantages__paragraph"><?= GetMessage("ADVANTAGES_TOP"); ?></p>
                <h3><?= GetMessage("CLIENTS_HELP"); ?></h3>
                <p class="advantages__paragraph"><?= GetMessage("ADVANTAGES_BOTTOM"); ?></p>
            </div>
        </div>
    </div>
</div>
<div class="callback">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 map">
            <div class="location">
                <h3><?= GetMessage("FIND_US"); ?>?</h3>
                <p class="location__address"><?= GetMessage("ADDRESS"); ?></p>
                <p class="location__phone"><?= GetMessage("PHONE"); ?></p>
            </div>
            <div id="map">
            </div>
        </div>
    </div>
</div>