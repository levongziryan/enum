<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendor/select2/dist/css/select2.min.css");
?>

<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-form main-form">
                <div class="modal-header">
                    <h2 class="modal-form-header"><?= GetMessage("REQUEST_ORDER"); ?></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="cross">&times;</span>
                    </button>
                </div>
                <p id="feedback_text_popup" style="display: none; text-align: center;"><?= GetMessage("THANKS"); ?></p>
                <div id="button_popup_close" class="button-block" style="display: none">
                    <button id="popup-close" data-dismiss="modal" type="submit"><?= GetMessage("OK"); ?></button>
                </div>
                <form class="modal-form" method="POST" id="popup-form">
                    <input type="email" name="email" id="email" placeholder="<?= GetMessage("EMAIL"); ?>">
                    <input type="text" name="name" id="name" placeholder="<?= GetMessage("NAME"); ?>">
                    <input type="hidden" name="type" id="type" value="<?= GetMessage("TYPE"); ?>">
                    <textarea name="message" id="message" placeholder="<?= GetMessage("MESSAGE"); ?>"></textarea>
                    <div class="button-block"><button id="popup-send" type="submit"><?= GetMessage("SEND"); ?></button></div>
                </form>
            </div>
        </div>
    </div>
</div>