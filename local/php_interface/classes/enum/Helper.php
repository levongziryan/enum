<?php
namespace enum;


class Helper
{

    public static function getOGinfo($code){
        \CModule::IncludeModule('iblock');
        $block = \CIBlockElement::GetList (array(), array("CODE" => $code), false, array())->fetch();
        $sitename = 'http://'.$_SERVER['HTTP_HOST'];
        $securesitename = "https://" . $_SERVER['HTTP_HOST'];
        $file = \CFile::GetFileArray($block["PREVIEW_PICTURE"]);

        $title = $block["NAME"];
        $image = $sitename.$file["SRC"];
        $imageSecure = $securesitename.$file["SRC"];
        $width = $file["WIDTH"];
        $height = $file["HEIGHT"];
        $description = $block["PREVIEW_TEXT"];
        $url = $sitename.$code;
        return array("TITLE" => $title,
                    "IMAGE" => $image,
                    "IMAGE_SECURE" => $imageSecure,
                    "WIDTH" => $width,
                    "HEIGHT" => $height,
                    "DESCRIPTION" => $description,
                    "URL" => $url);
    }

}