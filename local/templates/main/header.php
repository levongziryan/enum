<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use Bitrix\Main;
use Bitrix\Main\IO;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Text\BinaryString;
use Bitrix\Main\Page\Asset; ?>
    <!DOCTYPE html>
    <html lang="ru" xmlns:og="http://opengraphprotocol.org/schema/">
    <!--<![endif]-->
    <head>
        <title><? $APPLICATION->ShowTitle(); ?></title>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery/jquery.min.js"></script>
        <script src="https://api-maps.yandex.ru/2.1/?apikey=58c040d6-455c-4c7b-a1ce-7fa42202122e&lang=ru_RU"
                type="text/javascript"></script>
        <?
        Asset::getInstance()->addString("<meta charset=\"utf-8\">");
        Asset::getInstance()->addString("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
        Asset::getInstance()->addString("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
        ?>

        <?
        $APPLICATION->ShowHead();
        $APPLICATION->ShowMeta("keywords_inner");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendor/bootstrap/css/bootstrap.min.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendor/fontawesome/css/all.min.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/fonts/Helvetica/stylesheet.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/fonts/Museo/stylesheet.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/style.css");
        Asset::getInstance()->addString("<link rel=\"stylesheet\" type=\"text/css\" href=\"/local/templates/main/vendor/slick/slick.css\"/>");
        Asset::getInstance()->addString("<link rel=\"stylesheet\" type=\"text/css\" href=\"/local/templates/main/vendor/slick/slick-theme.css\"/>");
        Asset::getInstance()->addString("<link href=\"/local/templates/main/vendor/fontawesome/css/all.css\" type=\"text/css\" rel=\"stylesheet\">");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/template_styles.css");
        ?>
    </head>
<body>
<?
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "sidebar.menu.block",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/sidebar_menu_block.php"
    )
);
?>
    <!-- WORK AREA -->

<div class="main-wrapper <? $APPLICATION->ShowViewContent('class'); ?>">
<? $APPLICATION->ShowPanel(); ?>