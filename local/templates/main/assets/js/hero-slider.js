$('.hero-slider').slick({
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  adaptiveHeight: true,
  autoplay: true,
  autoplaySpeed: 4000,

  nextArrow: '<img class="right-arrow" src="assets/img/Arrow Right.svg" alt="">',
  prevArrow: '<img class="left-arrow" src="assets/img/Arrow Left.svg" alt="">',
  responsive: [
    
    {

      breakpoint: 1440,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:true,
        dots: true
      }
    },
    {
      breakpoint: 1200,
      settings:  {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings:  {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.modal-page').on('hidden.bs.modal', function (e) {
     $('.hero-slider').slick("unslick");
    $('.hero-slider').slick({
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      adaptiveHeight: true,
      autoplay: true,
      autoplaySpeed: 4000,

      nextArrow: '<img class="right-arrow" src="assets/img/Arrow Right.svg" alt="">',
      prevArrow: '<img class="left-arrow" src="assets/img/Arrow Left.svg" alt="">',
      responsive: [
        
        {

          breakpoint: 1440,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:true,
            dots: true
          }
        },
        {
          breakpoint: 1200,
          settings:  {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:false,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings:  {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:false,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:false,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  });