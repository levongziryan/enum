function unBindSlick() {
    $('.scroller').slick("unslick");
}

function bindSlick() {
    $('.scroller').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 4000,
        nextArrow: '<img class="right-arrow" src="/local/templates/main/assets/img/Arrow Right.svg" alt="">',
        prevArrow: '<img class="left-arrow" src="/local/templates/main/assets/img/Arrow Left.svg" alt="">',
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                    variableWidth: true,
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    variableWidth: true,
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    variableWidth: false,
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    variableWidth: false,
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                    adaptiveHeight: false
                }
            }
        ]
    });
}

$(document).ready(function () {
    if ($('.scroller').not('.slick-initialized')) {
        bindSlick();
    } else {
        unBindSlick();
        bindSlick();
    }
    ;
});