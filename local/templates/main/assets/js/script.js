function setServiceType(obj) {
    setTimeout(function () {
        $("#type").val(obj.dataset.type);
    }, 500);
}

var blocker = $(".blocker");
$(".blocker").on("click", function (event) {
    blocker.toggleClass("hide");
});

$('#ModalCenter').on('shown.bs.modal', function (e) {
    $(".services__modal-page").modal('hide');
});
$('#ModalCenter').on('hidden.bs.modal', function () {
    $('.services__modal-page').modal('show');
})

$('.menu-btn').on('click', function (e) {
    e.preventDefault;
    $(this).toggleClass('menu-btn_active');
    $(".sidebar").toggleClass('active');
});
if ($('.testimonials__wrapper').length > 0) {
    $('.testimonials__wrapper').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
    })
}

if ($(window).width() < 480) {
    setSlick();
}
$(window).resize(function(){
    if ($(window).width() < 480) {
        if ($('.slick').not('.slick-initialized')) {
            setSlick();
        }else{
            $('.slick').slick('unslick');
            setSlick();
        }
    }else{
        $('.slick').slick('unslick');
    }
})

function setSlick() {
    $('.slick').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000
    });
}