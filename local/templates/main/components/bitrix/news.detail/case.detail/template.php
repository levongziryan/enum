<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $APPLICATION;
$APPLICATION->RestartBuffer();
?>

<div class="modal__header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="case-header"><?= $arResult["NAME"] ?></div>
    <h2><?= $arResult["DETAIL_TEXT"]; ?></h2>
</div>
<div class="scroller">
    <? foreach ($arResult['DISPLAY_PROPERTIES']['detail_pictures']['VALUE'] as $value): ?>
        <div class="scroller__slide slide-1">
            <img src="<?= CFile::GetPath($value) ?>" alt="">
        </div>
    <? endforeach; ?>
</div>
<script>
    $(document).ready(function () {
        $('.scroller').slick({
            dots: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            autoplay: true,
            autoplaySpeed: 4000,
            nextArrow: '<img class="right-arrow" src="/local/templates/main/assets/img/Arrow Right.svg" alt="">',
            prevArrow: '<img class="left-arrow" src="/local/templates/main/assets/img/Arrow Left.svg" alt="">',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        variableWidth: true,
                        centerMode: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:true
                    }
                },
                {
                    breakpoint: 1200,
                    settings:  {
                        variableWidth: true,
                        centerMode: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:false,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings:  {
                        variableWidth: false,
                        centerMode: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:false,
                        dots: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        variableWidth: false,
                        centerMode: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:false,
                        dots: true,
                        adaptiveHeight: false
                    }
                }
            ]
        });
    });
</script>