<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $APPLICATION;
$APPLICATION->RestartBuffer();
if (empty($arResult['DISPLAY_PROPERTIES']['taryphs']['VALUE'])) {
    $filter = -1;
} else {
    $filter = $arResult['DISPLAY_PROPERTIES']['taryphs']['VALUE'];
}
$GLOBALS['ATTACHED'] = array("=ID" => $filter);
?>

    <div class="modal__header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h1><?= $arResult["NAME"] ?></h1>
        <h2><?= $arResult['DISPLAY_PROPERTIES']['title']['VALUE'] ?></h2>
        <p><?= $arResult["DETAIL_TEXT"]; ?></p>
        <div class="share-wrapper">
            <p>Поделиться:</p>
            <div>
                <a class="share-link" onclick="Share.vkontakte('http://<?= $_SERVER['HTTP_HOST'] ?>/services/#<?= $arResult["CODE"] ?>', '<?= $arResult["NAME"] ?>', '<?= 'http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PICTURE"]["SRC"]; ?>', '<?= $arResult["PREVIEW_TEXT"]; ?>');return false;"
                   href="#">ВКонтакте </a>
                <a class="share-link" onclick="Share.facebook('http://<?= $_SERVER['HTTP_HOST'] ?>/services/#<?= $arResult["CODE"] ?>', '<?= $arResult["NAME"] ?>', '<?= 'http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PICTURE"]["SRC"]; ?>', '<?= $arResult["PREVIEW_TEXT"]; ?>');return false;"
                   href="#">Facebook </a>
                <a class="share-link" onclick="Share.twitter('http://<?= $_SERVER['HTTP_HOST'] ?>/services/#<?= $arResult["CODE"] ?>', '<?= $arResult["NAME"] ?>');return false;"
                   href="#">Twitter </a>
                <a class="share-link" onclick="Share.telegram('http://<?= $_SERVER['HTTP_HOST'] ?>/services/#<?= $arResult["CODE"] ?>', '<?= $arResult["NAME"] ?>', '<?= 'http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PICTURE"]["SRC"]; ?>', '<?= $arResult["PREVIEW_TEXT"]; ?>');return false;"
                   href="#">Telegram </a>

                <!--<a class="share-link" onclick="Share.odnoklassniki('http://<?= $_SERVER['HTTP_HOST'] ?>/services/#<?= $arResult["CODE"] ?>', '<?= $arResult["PREVIEW_TEXT"]; ?>');return false;"
           href="#">Однаклассники</a>
        <a class="share-link" onclick="Share.mailru('http://<?= $_SERVER['HTTP_HOST'] ?>/services/#<?= $arResult["CODE"] ?>', '<?= $arResult["NAME"] ?>', '<?= 'http://' . $_SERVER['HTTP_HOST'] . $arResult["DETAIL_PICTURE"]["SRC"]; ?>', '<?= $arResult["PREVIEW_TEXT"]; ?>');return false;"
           href="#">Mail.ru </a>-->
            </div>
        </div>
    </div>
<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "taryphs.list",
    Array(
        "SERVICE_NAME" => $arResult["NAME"],
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "N",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("", ""),
        "FILTER_NAME" => "ATTACHED",
        "USE_FILTER" => "Y",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "14",
        "IBLOCK_TYPE" => "attend",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "3",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("price", "time_count", "time_word"),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "DATE",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
); ?>