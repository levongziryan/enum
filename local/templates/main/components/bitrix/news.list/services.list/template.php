<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);
?>

<div class="services">
    <div class="row">
        <? foreach ($arResult["ITEMS"] as $k => $arItem) {?>
        <div class="services__block col-xl-3 col-lg-6">
            <h2><?= $arItem["NAME"] ?></h2>
            <p><?= $arItem["DETAIL_TEXT"] ?></p>
            <a class="services__link learn-more" href="" data-element-code="<?= $arItem["CODE"]?>" data-toggle="modal"
               data-target=".modal-page"><?=GetMessage("DETAILS");?></a>
        </div>
        <?}?>
    </div>
</div>