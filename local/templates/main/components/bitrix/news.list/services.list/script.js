$(document).ready(function(){
if(window.location.hash != ''){
        let code = window.location.hash.slice(1);
        $.ajax({
            type: "POST",
            url: "/ajax/callDetailService.php",
            data: {ELEMENT_CODE: code},
            success: function (res) {
                if(res != 'ERROR') {
                    $(".services__modal-page .main-wrapper").html(res);
                    $('.modal-page').modal('show');
                }else{
                    let newPath = window.location.href.replace(window.location.hash, '');
                    history.pushState(null, null, newPath);
                }
            }
        });
    }
    $(".learn-more").click(function(e){
        let element = this;
        let prefHref = window.location.href;
        $.ajax({
            type: "POST",
            url: "/ajax/callDetailService.php",
            data: {ELEMENT_CODE: element.dataset.elementCode},
            success: function (res) {
                if(res != 'ERROR') {
                    $(".services__modal-page .main-wrapper").html(res);
                    window.location.hash = element.dataset.elementCode;
                }else{
                    let newPath = window.location.href.replace(window.location.hash, '');
                    history.pushState(null, null, newPath);
                }
            }
        });
    })
    $('.modal-page').on('hide.bs.modal', function () {
        let newPath = window.location.href.replace(window.location.hash, '');
        history.pushState(null, null, newPath);
    })
})

Share = {
    vkontakte: function(purl, ptitle, pimg, text) {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    odnoklassniki: function(purl, text) {
        url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
        url += '&st.comments=' + encodeURIComponent(text);
        url += '&st._surl='    + encodeURIComponent(purl);
        Share.popup(url);
    },
    facebook: function(purl, ptitle, pimg, text) {
        url  = 'http://m.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&u='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    mailru: function(purl, ptitle, pimg, text) {
        url  = 'http://connect.mail.ru/share?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&imageurl='    + encodeURIComponent(pimg);
        Share.popup(url)
    },
    telegram: function(purl, ptitle, pimg, text) {
        url  = 'https://telegram.me/share/url?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&text='        + encodeURIComponent(ptitle);
        Share.popup(url)
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};