<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);
?>


<div class="hero d-flex hero-slider">
    <? foreach ($arResult["ITEMS"] as $arItem):
        if ($arItem['DISPLAY_PROPERTIES']['show_in_slider']['VALUE']) {
            ?>
            <div class="hero__slide startscreen" style="background: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>);">
                <div class="slider-header"><?= $arItem['NAME'] ?></div>
                <h2><?= $arItem['DETAIL_TEXT'] ?></h2>
                <button onclick="loadModal(this);" data-target=".modal-page" class="open-modal hero__button mr-auto"
                        data-toggle="modal" data-element-code="<?= $arItem["CODE"] ?>"><?=GetMessage("CASE");?>
                </button>
            </div>
        <? } ?>
    <? endforeach; ?>
</div>