<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);
?>

<div class="testimonials">
    <h2><?=GetMessage("REVIEWS");?></h2>
    <div class="testimonials__wrapper">
        <? foreach ($arResult["ITEMS"] as $k => $arItem) {
            if(($k+1)%3 == 1){?>
            <div class="testimonials__slide" style="height: auto !important;">
                <div class="row">
             <?}?>
                    <div class="testimonials__item col-xl-4 col-lg-4 col-md-12">
                        <div class="testimonials__person">
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>">
                            <div class="testimonials_username">
                                <h3><?= $arItem["NAME"] ?></h3>
                                <p><?= $arItem["PREVIEW_TEXT"] ?></p>
                            </div>
                        </div>
                        <p class="testimonials__paragraph"><?= $arItem["DETAIL_TEXT"]; ?></p>
                    </div>
            <?if(($k+1)%3 == 0 || (empty($arResult["ITEMS"][$k+1]))){?>
                </div>
            </div>
            <?}?>
        <?}?>
    </div>
</div>