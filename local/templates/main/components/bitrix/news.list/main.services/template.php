<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="services">
    <div class="adaptive-box">
        <div class="row slick">
            <? foreach ($arResult["ITEMS"] as $k => $arItem) {?>
            <div class="services__item col-xl-3 col-lg-6 col-md-6 col-sm-6" style="height: auto !important;">
                <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>">
                <h3><?= $arItem["NAME"] ?></h3>
                <p><?= $arItem["PREVIEW_TEXT"] ?></p>
            </div>
            <?}?>
        </div>
    </div>
</div>