$(document).ready(function(){
    if(window.location.hash != ''){
        let code = window.location.hash.slice(1);
        $.ajax({
            type: "POST",
            url: "/ajax/callDetailCase.php",
            data: {ELEMENT_CODE: code},
            success: function (res) {
                if(res != 'ERROR') {
                    $('.scroller').slick("unslick");
                    $(".portfolio-modal .main-wrapper").html(res);
                    $('.modal-page').modal('show');
                }else{
                    let newPath = window.location.href.replace(window.location.hash, '');
                    history.pushState(null, null, newPath);
                }
            }
        });
    }
    $('.modal-page').on('hide.bs.modal', function () {
        $(".portfolio-modal .main-wrapper").html("");
        let newPath = window.location.href.replace(window.location.hash, '');
        history.pushState(null, null, newPath);
    })
})

function loadModal(element){
    $.ajax({
        type: "POST",
        url: "/ajax/callDetailCase.php",
        data: {ELEMENT_CODE: element.dataset.elementCode},
        success: function (res) {
            if(res != 'ERROR') {
                $('.scroller').slick("unslick");
                $(".portfolio-modal .main-wrapper").html(res);
                window.location.hash = element.dataset.elementCode;
            }else{
                let newPath = window.location.href.replace(window.location.hash, '');
                history.pushState(null, null, newPath);
            }
        }
    });
}