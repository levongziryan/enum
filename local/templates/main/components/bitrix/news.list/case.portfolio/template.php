<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


    <?foreach ($arResult["ITEMS"] as $arItem):?>
        <div onclick="loadModal(this);" class="cases__item open-modal" id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
             data-target=".modal-page" data-toggle="modal" data-element-code="<?= $arItem["CODE"]?>">
            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" class=".cases-item  mix category-a">
        </div>
    <? endforeach; ?>

