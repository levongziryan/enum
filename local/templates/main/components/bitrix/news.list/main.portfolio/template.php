<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);
?>

<div class="portfolio">
    <div class="adaptive-box">
        <h2 class="block-header"><?=GetMessage("MAIN_PORTFOLIO");?></h2>
        <div class="row">
            <? foreach ($arResult["ITEMS"] as $k => $arItem) {
                if ($k % 2 != 1){ ?>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 sm-img-adapt">
                        <picture>
                            <source media="(min-width: 768px)"
                                    srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                            <source media="(max-width: 767px)" srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                            <a href="/portfolio/#<?=$arItem["CODE"]?>">
                            <img class="cases-item__img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                 alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"/>
                            </a>
                        </picture>
                        <?if(empty($arResult["ITEMS"][$k+1])){?>
                            </div>
                        <?}
                }else{ ?>
                        <picture>
                            <source media="(min-width: 768px)"
                                    srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                            <source media="(max-width: 767px)" srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                            <a href="/portfolio/#<?=$arItem["CODE"]?>">
                            <img class="cases-item__img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                 alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"/>
                            </a>
                        </picture>
                    </div>
                <? }
            } ?>
        </div>
        <a href="/portfolio/" class="portfolio-link"><?=GetMessage("WATCH_PORTFOLIO");?></a>
    </div>
</div>