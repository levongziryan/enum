<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);
?>
<div class="choose-service">
    <div class="row">
        <? foreach ($arResult["ITEMS"] as $k => $arItem) {?>
        <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="service">
                <h2><?= $arItem["NAME"] ?></h2>
                <p class="description"><?= $arItem["DETAIL_TEXT"] ?></p>
                <p class="cost">От <span><?=$arItem['DISPLAY_PROPERTIES']['price']['VALUE']?> ₽</span></p>
                <button class="sidebar__button btn service-order" type="button" onclick="setServiceType(this)"
                        data-type="<? echo "Тип услуги: ".$arParams["SERVICE_NAME"].". По тарифу: ".$arItem["NAME"]?>"
                        data-toggle="modal" data-target="#ModalCenter"><?=GetMessage("ORDER_ITEM");?>
                </button>
                <p class="time"><span><?=$arItem['DISPLAY_PROPERTIES']['time_count']['VALUE']?></span><?=$arItem['DISPLAY_PROPERTIES']['time_word']['VALUE']?></p>
            </div>
        </div>
        <?}?>
    </div>
</div>