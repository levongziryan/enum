$(document).ready(function(){
    $("button[id=contacts-send]").click(function(e){
        e.preventDefault();
        let send = true;
        if(!checkField($("input[id=name_contacts]"), "field")) send = false;
        if(!checkField($("input[id=email_contacts]"), "email")) send = false;
        if(!checkField($("textarea[id=message_contacts]"), "field")) send = false;
        if(!checkField($("select[id=select]"), "select")) send = false;
        if(send){
            $.ajax({
                type: "POST",
                url: "/ajax/sendMailAboutRequest.php",
                data: $("form[id=contacts-form]").serialize(),
                success: function (res) {
                    if(res == "OK"){
                        $("#feedback_text_contacts").css("display", "block");
                        $("form[id=contacts-form]").css("display", "none");
                        $("#order_contacts").text('Enum');
                    }
                }
            });
        };
    })
})

function checkField(elem, type){
    if(type == 'field'){
        if(elem.val().trim() == ''){
            elem.css("border-color", "#f00");
            return false;
        }else{
            elem.css("border-color", "");
            return true;
        }
    }else if(type == 'email'){
        let re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(!re.test(elem.val())){
            elem.css("border-color", "#f00");
            return false;
        }else{
            elem.css("border-color", "");
            return true;
        }
    }else{
        if(elem.val()){
            $(".select2-selection").css("border-color", "");
            return true;
        }else{
            $(".select2-selection").css("border-color", "#f00");
            return false;
        }
    }
}