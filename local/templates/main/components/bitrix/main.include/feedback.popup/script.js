$(document).ready(function(){
    $("button[id=popup-send]").click(function(e){
        e.preventDefault();
        let send = true;
        if(!checkField($("input[id=name]"), "field")) send = false;
        if(!checkField($("input[id=email]"), "email")) send = false;
        if(!checkField($("textarea[id=message]"), "field")) send = false;
        if(send){
            $.ajax({
                type: "POST",
                url: "/ajax/sendMailAboutRequest.php",
                data: $("form[id=popup-form]").serialize(),
                success: function (res) {
                    if(res == "OK"){
                        $("form[id=popup-form]").css("display", "none");
                        $(".modal-form-header").text("Enum");
                        $("#feedback_text_popup").css("display", "block");
                        $("#button_popup_close").css("display", "block");
                    }
                }
            });
        }
    })
})

function checkField(elem, type){
    if(type == 'field'){
        if(elem.val().trim() == ''){
            elem.css("border-color", "#f00");
            return false;
        }else{
            elem.css("border-color", "");
            return true;
        }
    }else{
        let re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(!re.test(elem.val())){
            elem.css("border-color", "#f00");
            return false;
        }else{
            elem.css("border-color", "");
            return true;
        }
    }
}