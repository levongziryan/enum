$(document).ready(function () {
    $(".filter__button").click(function(){
        let current = this;
        if(!current.classList.contains('active')){
            $.ajax({
                type: "POST",
                url: "/ajax/changePortfolioList.php",
                data: {filter: current.dataset.filter},
                success: function (res) {
                    $(".filter__button.active").removeClass('active');
                    current.classList.add('active');
                    $(".gallery").html(res);
                }
            });
        }
    })
})
