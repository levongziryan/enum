ymaps.ready(init);
            function init(){ 
                // Создание карты.    
                var myMap = new ymaps.Map("map", {
                    // Координаты центра карты.
                    // Порядок по умолчанию: «широта, долгота».
                    // Чтобы не определять координаты центра карты вручную,
                    // воспользуйтесь инструментом Определение координат.
                    center: [55.4841, 28.777467],
                    // Уровень масштабирования. Допустимые значения:
                    // от 0 (весь мир) до 19.
                    zoom: 17,
                    controls: ['zoomControl']
                }, {
            searchControlProvider: 'yandex#search'
        });
                
                  // Создаем ломаную, используя класс GeoObject.
    var myGeoObject = new ymaps.GeoObject({
            // Описываем геометрию геообъекта.
            geometry: {
                // Тип геометрии - "Ломаная линия".
                type: "LineString",
                // Указываем координаты вершин ломаной.
                coordinates: [
                    [55.486230,28.778561],
                    [55.485121,28.777407]
                ]
            },
            // Описываем свойства геообъекта.
            properties:{
                // Содержимое хинта.
                hintContent: "Я геообъект",
                // Содержимое балуна.
                balloonContent: "Меня можно перетащить"
            }
        }, {
            // Задаем опции геообъекта.
            // Включаем возможность перетаскивания ломаной.
            draggable: true,
            // Цвет линии.
            strokeColor: "red",
            // Ширина линии.
            strokeWidth: 5
        });
                var myPolyline = new ymaps.Polyline([
            // Указываем координаты вершин ломаной.
            [55.486230,28.778561],
            [55.484739,28.778194],
            [55.484776,28.777622],
            [55.484990,28.777077]
        ], {
            // Описываем свойства геообъекта.
            // Содержимое балуна.
            balloonContent: "Ломаная линия"
        }, {
            // Задаем опции геообъекта.
            // Отключаем кнопку закрытия балуна.
            balloonCloseButton: false,
            // Цвет линии.
            strokeColor: "#ff0303",
            // Ширина линии.
            strokeWidth: 5,
            // Коэффициент прозрачности.
            strokeOpacity: 1
        });




        
        myMap.geoObjects
           .add(myPolyline)
           .add(new ymaps.Placemark([55.484990,28.777077], {
            balloonContent: '<strong>Мы тут</strong>'
        }, {
            preset: 'islands#dotIcon',
            iconColor: '#F2C94C'
        }))
    }