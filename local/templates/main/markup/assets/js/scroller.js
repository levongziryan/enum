

// var isSlickLoaded = (typeof $.fn.Slick !== 'undefined');


// if(isSlickLoaded){}
  $('.scroller').slick({
  dots: true,
  infinite: true,
  speed: 1000,
  slidesToShow: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  nextArrow: '<img class="right-arrow" src="assets/img/Arrow Right.svg" alt="">',
  prevArrow: '<img class="left-arrow" src="assets/img/Arrow Left.svg" alt="">',
  responsive: [
    {
      breakpoint: 1440,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:true
      }
    },
    {
      breakpoint: 1200,
      settings:  {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings:  {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true,
        adaptiveHeight: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

var myCarousel = $('.scroller');
$('.modal-page').on('shown.bs.modal', function (e) {
     $('.scroller').slick("unslick");
     $('.scroller').slick({
  dots: true,
  infinite: true,
  speed: 1000,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  autoplay: true,
  autoplaySpeed: 4000,
  nextArrow: '<img class="right-arrow" src="assets/img/Arrow Right.svg" alt="">',
  prevArrow: '<img class="left-arrow" src="assets/img/Arrow Left.svg" alt="">',
  responsive: [
    {
      breakpoint: 1440,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:true
      }
    },
    {
      breakpoint: 1200,
      settings:  {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings:  {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        dots: true,
        adaptiveHeight: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
 });


 

