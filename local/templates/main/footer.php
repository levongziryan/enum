<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\IO;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Text\BinaryString;
use Bitrix\Main\Page\Asset;

global $APPLICATION;

$title = $APPLICATION->GetProperty("og:title");
$image = $APPLICATION->GetProperty("og:image");
$image_type = $APPLICATION->GetProperty("og:image:type");
$image_width = $APPLICATION->GetProperty("og:image:width");
$image_height = $APPLICATION->GetProperty("og:image:height");
$description = $APPLICATION->GetProperty("og:description");
$site_name = $APPLICATION->GetProperty("og:site_name");
$url = $APPLICATION->GetProperty("og:url");
$locale = $APPLICATION->GetProperty("og:locale");
$type = $APPLICATION->GetProperty("og:type");

$APPLICATION->AddHeadString('<meta property="og:title" content="' . $title . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:image" content="' . $image . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:image:type" content="' . $image_type . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:image:width" content="' . $image_width . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:image:height" content="' . $image_height . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:description" content="' . $description . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:site_name" content="' . $site_name . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:url" content="' . $url . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:locale" content="' . $locale . '"/>', true);
$APPLICATION->AddHeadString('<meta property="og:type" content="' . $type . '"/>', true);
?>
</div>

<!--!WORK AREA -->

</body>
<!-- Modal -->

<?
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "feedback.popup",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/feedback_popup.php"
    )
);
?>

<script type="text/javascript" src=" https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/vendor/slick/slick.min.js"></script>
<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/"></script>-->
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/select2/dist/js/select2.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/js/select.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/js/script.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/js/hero-slider.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/js/scroller.js"></script>-
<? if ($APPLICATION->GetCurDir() == "/contacts/") { ?>
    <script src="<?= SITE_TEMPLATE_PATH ?>/assets/js/ymaps-contacts.js"></script>
<? }
if ($APPLICATION->GetCurDir() == "/") {?>
    <script src="<?= SITE_TEMPLATE_PATH ?>/assets/js/ymaps.js"></script>
<? } ?>
</html>
