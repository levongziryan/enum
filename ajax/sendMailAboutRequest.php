<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

$name = $_POST['name'];
$email = $_POST['email'];
$type = $_POST['type'];
$message = $_POST['message'];

if(!empty($name) && !empty($email) && !empty($type) && !empty($message)){
    $message_text =  array("VALUE" => array("TEXT" => $message, "TYPE" => "text"));
    $PROPS = array("32" => $email, "33" => $name, "34" => $message_text, "35" => $type);
    $fields = array(
        'IBLOCK_ID' => 10,
        'NAME' => 'Заявка от '.$email,
        'PROPERTY_VALUES' => $PROPS
    );
    $element = new CIBlockElement;
    $blockID = $element->Add($fields);
    $site = CSite::GetByID(SITE_ID)->Fetch()['NAME'];
    $mailFields = array(
        'DEFAULT_EMAIL_FROM' => COption::GetOptionString("main", "email_from"),
        'EMAIL_TO' => COption::GetOptionString("main", "email_from"),
        'AUTHOR_EMAIL' => $email,
        'SITE_NAME' => $site,
        'AUTHOR' => $name,
        'TEXT' => 'Тип услуги: "'.$type.'". '.$message
    );
    $mailPROPS = array('FEEDBACK_FORM', SITE_ID, $mailFields, 'N', 10);
    $sendID = CEvent::Send('FEEDBACK_FORM', SITE_ID, $mailFields, 'N', 10);

    if(!empty($sendID) && !empty($blockID)){
        echo "OK";
    } else{
        echo "ERROR";
    }
} else{
    echo "ERROR";
}
?>
