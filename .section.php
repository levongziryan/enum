<?
use enum\Helper;
global $APPLICATION;
$result = Helper::getOGinfo(CMain::GetCurDir());
$sSectionName = "Главная";
$arDirProperties = array(
    "title" => $result["TITLE"],
    "description" => $result["DESCRIPTION"],
    "keywords" => "Enum, bitrix, bitrix24, сайты, web, веб, CRM, сайт, crm, Bitrix",
    "robots" => "index, follow",
    "og:title" => $result["TITLE"],
    "og:image" => $result["IMAGE"],
    "og:image:secure_url" => $result["IMAGE_SECURE"],
    "og:image:type" => "image/jpeg",
    "og:image:width" => $result["WIDTH"],
    "og:image:height" => $result["HEIGHT"],
    "og:description" => $result["DESCRIPTION"],
    "og:site_name" => "ENUM.BY",
    "og:url" => $result["URL"],
    "og:locale" => "ru_RU",
    "og:type" => "website",
);
?>